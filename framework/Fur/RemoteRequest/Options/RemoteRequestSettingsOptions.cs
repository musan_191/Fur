﻿using Fur.ConfigurableOptions;

namespace Fur.RemoteRequest
{
    /// <summary>
    /// 远程请求设置
    /// </summary>
    [OptionsSettings("RemoteRequestSettings")]
    public sealed class RemoteRequestSettingsOptions : IConfigurableOptions
    {
    }
}